FROM docker-images.jobtechdev.se/jobtech-debian-slim-stable-base-image/jobtech-debian-slim-stable-base-image

WORKDIR /usr/local/bin

RUN apt-get -y update && apt-get -y upgrade &&\
        apt-get -y install wget ca-certificates &&\
        wget https://dl.min.io/client/mc/release/linux-amd64/mc &&\
        chmod +x mc

CMD [ "/usr/local/bin/mc" ]
